from faker import Faker
from utils.gorest_handler import GoRESTHandler

gorest_handler = GoRESTHandler()


# TWORZENIE
def test_create_user():
    user_data = {
        "name": Faker().name(),
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user_by_id(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    # UPDATE
    user_data_updated = {
        "name": Faker().name(),
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    user_id = body["id"]
    body = gorest_handler.update_user(user_data_updated, user_id).json()

    assert body["name"] == user_data_updated["name"]
    assert body["gender"] == user_data_updated["gender"]
    assert body["email"] == user_data_updated["email"]
    assert body["status"] == user_data_updated["status"]
    # USUNIĘCIE
    response = gorest_handler.delete_user(user_id)
